package org.dromara.workflow.mapper;

import org.dromara.workflow.domain.WfCategory;
import org.dromara.workflow.domain.vo.WfCategoryVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 流程分类Mapper接口
 *
 * @author may
 * @date 2023-06-27
 */
public interface WfCategoryMapper extends BaseMapperPlus<WfCategory, WfCategoryVo> {

}
