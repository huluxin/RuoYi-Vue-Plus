package org.dromara.workflow.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.workflow.domain.WfForm;
import org.dromara.workflow.domain.vo.WfFormVo;

/**
 * 流程表单Mapper接口
 *
 * @author KonBAI
 */
public interface WfFormMapper extends BaseMapperPlus<WfForm, WfFormVo> {
}
