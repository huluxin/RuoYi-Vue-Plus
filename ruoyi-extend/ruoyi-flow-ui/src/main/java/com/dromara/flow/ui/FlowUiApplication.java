package com.dromara.flow.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * flowable-ui启动类
 * Author: 土豆仙
 */
@SpringBootApplication
public class FlowUiApplication {

    public static void main(String[] args) {
        SpringApplication.run(com.dromara.flow.ui.FlowUiApplication.class, args);
    }

}
